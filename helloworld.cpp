#include <iostream>
#include <string>

using namespace std;

int main()
{
    int CycleTest = 1;

    
        float fTempValue; //Изначальное значение температуры
        float fTempValueNEW; //Новое значение температуры
        string sTypeTemp; //Изначальная шкала
        string sNewTypeTemp; //Новая шкала

        cout << "\n Input temperature \n";
        cin >> fTempValue; //Ввод температуры
        cout << "Input original type of temperature (Celcius , Kelvin, Farenheit) \n";
        cin >> sTypeTemp; //Ввод названия первоначальной шкалы
        cout << "Input new type of temperature (Celcius , Kelvin, Farenheit) \n";
        cin >> sNewTypeTemp; //Ввод названия новой шкалы

        if (sTypeTemp == "Celcius") {

            if (sNewTypeTemp == "Farenheit") {
                fTempValueNEW = (fTempValue * (9 / 5)) + 32;
                cout << fTempValueNEW;
            }

            if (sNewTypeTemp == "Kelvin") {
                fTempValueNEW = fTempValue + 273.15;
                cout << fTempValueNEW;
            }
        }

        if (sTypeTemp == "Farenheit") {

            if (sNewTypeTemp == "Celcius") {
                fTempValueNEW = (fTempValue - 32) * (5 / 9);
                cout << fTempValueNEW;
            }

            if (sNewTypeTemp == "Kelvin") {
                fTempValueNEW = (fTempValue - 32) * (5 / 9) + 273.15;
                cout << fTempValueNEW;
            }
        }

        if (sTypeTemp == "Kelvin") {

            if (sNewTypeTemp == "Celcius") {
                fTempValueNEW = fTempValue - 273.15;
                cout << fTempValueNEW;
            }

            if (sNewTypeTemp == "Farenheit") {
                fTempValueNEW = (fTempValue - 273.15) * (9 / 5) + 32;
                cout << fTempValueNEW;
            }
        }
    
}
